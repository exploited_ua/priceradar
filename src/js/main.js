//= ../../node_modules/jquery/dist/jquery.js
//= ../../node_modules/jquery-ui-dist/jquery-ui.js
//= ../../node_modules/swiper/dist/js/swiper.jquery.js
//= ../../node_modules/jquery-nice-select/js/jquery.nice-select.js

$(document).ready(function() {
    $('select').niceSelect();
});

var swiper = new Swiper('.market_slider', {
    scrollbar: '.swiper-scrollbar',
    scrollbarHide: false,
    slidesPerView: 'auto',
    centeredSlides: true,
    spaceBetween: 0,
    grabCursor: true,
    effect: 'coverflow',
    scrollbarDraggable: true,
    coverflow: {
        rotate: 0,
        stretch: -1,
        depth: 140,
        modifier: -1.5,
        slideShadows : false
    },
    onInit: function (swiper) {
        var center_slide = swiper.slides.length / 2;
        swiper.slideTo(center_slide);
    }
});

var galleryMain = new Swiper('.gallery-main', {
    nextButton: '.slider-btn-next',
    prevButton: '.slider-btn-prev',
    direction: 'vertical',
    spaceBetween: 10,
    loop:true,
    loopedSlides: 5 //looped slides should be the same
});
var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 20,
    slidesPerView: 3,
    direction: 'vertical',
    touchRatio: 0.2,
    loop:true,
    loopedSlides: 5, //looped slides should be the same
    slideToClickedSlide: true
});
galleryMain.params.control = galleryThumbs;
galleryThumbs.params.control = galleryMain;

var similarSlider = new Swiper('.similar_product_slider', {
    slidesPerView: 'auto',
    centeredSlides: false,
    spaceBetween: 0,
    nextButton: '.btn-next',
    prevButton: '.btn-prev'
});

var tagBlock = new Swiper('.tag_block', {
    scrollbar: '.tag-scrollbar',
    direction: 'vertical',
    slidesPerView: 'auto',
    mousewheelControl: true,
    scrollbarHide: false,
    scrollbarDraggable: true,
    freeMode: true

});

$('.filter_parameters_name').on('click', function (e) {
    e.preventDefault();
   $(this).next().slideToggle();
   $(this).parent().toggleClass('open');
});

$('.open_search').on('click', function (e) {
    e.preventDefault();
   $('.search_block').toggleClass('open');
   $('.header_wrap').toggleClass('search_opened');
});
$('.toggle_menu').on('click', function () {
   $(this).toggleClass('active');
   $('.main_nav_menu').toggleClass('open');
});
$('.open_filter').on('click', function () {
   $(this).toggleClass('active');
   $('.filter_section').toggleClass('open');
    $('.filter_top_container').removeClass('open');
    $('.open_category').removeClass('active');
});
$('.open_category').on('click', function () {
   $(this).toggleClass('active');
   $('.filter_top_container').toggleClass('open');
    $('.filter_section').removeClass('open');
    $('.open_filter').removeClass('active');
});

$('.tab_nav_item').on('click', function (e) {
    e.preventDefault();
    var id = $(this).attr('href');
    $('.tab_nav_item').removeClass('active');
    $('.tab_nav_item[href="'+ id +'"]').addClass('active');
    $('.tab_item').removeClass('open');
    $(id).addClass('open');
    similarSlider.update();
});
$(window).scroll(function () {
    if($(document).scrollTop() > 200){
        $('.nav_panel').addClass('open');
    }else{
        $('.nav_panel').removeClass('open');
    }
});
$('.tab_nav_item.top_nav').on('click', function (e) {
    e.preventDefault();
    var id = $(this).attr('href');

    similarSlider.update();
    var offset = $(id).offset();
    $(document).scrollTop(offset.top - 100);
});

$( function() {
    $( "#price-range" ).slider({
        range: true,
        min: 0,
        max: 1700000,
        values: [ 0, 1700000 ],
        slide: function( event, ui ) {
            $(".price-from").text( ui.values[ 0 ] + "₽");
            $(".price-to").text( ui.values[ 1 ]  + "₽");
        }
    });
    $( ".price-from" ).text( $( "#price-range" ).slider( "values", 0 ) + "₽");
    $( ".price-to" ).text( $( "#price-range" ).slider( "values", 1 ) + "₽");
} );
$( function() {
    $( "#size-range" ).slider({
        range: true,
        min: 2,
        max: 15,
        values: [ 0, 15 ],
        slide: function( event, ui ) {
            $(".size-from").text( ui.values[ 0 ]);
            $(".size-to").text( ui.values[ 1 ]);
        }
    });
    $( ".size-from" ).text( $( "#size-range" ).slider( "values", 0 ));
    $( ".size-to" ).text( $( "#size-range" ).slider( "values", 1 ));
} );

var sliderCamera = new Swiper('.slider_camera', {
    slidesPerView: 'auto',
    centeredSlides: false,
    spaceBetween: 0,
    grabCursor: true
});

var sliderWatch = new Swiper('.slider_watch', {
    slidesPerView: 'auto',
    centeredSlides: false,
    spaceBetween: 0,
    grabCursor: true
});

if ($(document).width() < 767){
    var sliderPhone = new Swiper('.phone_category_slider', {
        slidesPerView: 'auto',
        centeredSlides: false,
        spaceBetween: 0,
        grabCursor: true
    });
    var sliderTablets = new Swiper('.tablet_category_slider', {
        slidesPerView: 'auto',
        centeredSlides: false,
        spaceBetween: 0,
        grabCursor: true
    });
}

$('.search_input').focusin(function () {
    $('.search_hint').addClass('open');
});
$('.search_input').focusout(function () {
    $('.search_hint').removeClass('open');
});
